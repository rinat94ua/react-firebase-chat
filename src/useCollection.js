import { useState, useEffect } from 'react'
import { db } from './firebase'

function useCollection(path, orderBy, where=[]) {
  const [docs, setDocs] = useState([])

  useEffect(() => {
    let collection = db.collection(path)
    const [queryField, queryOperator, queryValue] = where

    if(orderBy) {
      collection = collection.orderBy(orderBy)
    }

    if(queryField) {
      collection = collection.where(queryField, queryOperator, queryValue)
    }

    return collection.onSnapshot(snapshot => {
      const docs = []
      snapshot.forEach(doc => {
        docs.push({
          ...doc.data(),
          id: doc.id
        })
      })
      setDocs(docs)
    })
  }, [path, orderBy])

  return docs
}

export { useCollection }
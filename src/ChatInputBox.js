import React from 'react'
import { db } from './firebase'

function ChatInputBox({ user, channelId }) {

  return (
    <form 
      onSubmit={evt => {
        evt.preventDefault()
        const value = evt.target.elements[0].value
        db
          .collection('channels')
          .doc(channelId)
          .collection('messages')
          .add({
            user: db.collection('users').doc(user.uid),
            text: value,
            createdAt: new Date()
          })

        evt.target.reset()
      }}
      className="ChatInputBox"
    >
      <input className="ChatInput" placeholder="Message #general" />
    </form>
  );
}

export default ChatInputBox;

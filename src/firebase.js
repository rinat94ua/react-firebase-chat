import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/database'
import 'firebase/auth'

const config = {
  apiKey: "AIzaSyAlBcRamEEMND7bl5_sNh58yLfIt6Tsj6g",
  authDomain: "react-chat-app-43971.firebaseapp.com",
  databaseURL: "https://react-chat-app-43971.firebaseio.com",
  projectId: "react-chat-app-43971",
  storageBucket: "react-chat-app-43971.appspot.com",
  messagingSenderId: "482230083316",
  appId: "1:482230083316:web:66b4e6df65d10d9ba4c8a1"
}
firebase.initializeApp(config)

const db = firebase.firestore()
const rtdb = firebase.database()

function setupPresence(user) {
  const isOfflineForRTDB = {
    state: 'offline',
    lastChanged: firebase.database.ServerValue.TIMESTAMP
  }
  const isOnlineForRTDB = {
    state: 'online',
    lastChanged: firebase.database.ServerValue.TIMESTAMP
  }
  const isOfflineForFirestore = {
    state: 'offline',
    lastChanged: firebase.firestore.FieldValue.serverTimestamp()
  }
  const isOnlineForFirestore = {
    state: 'online',
    lastChanged: firebase.firestore.FieldValue.serverTimestamp()
  }

  const rtdbRef = rtdb.ref(`/status/${user.uid}`)
  const userDoc = db.doc(`/users/${user.uid}`)

  rtdb.ref('.info/connected').on('value', async snapshot => {
    if (snapshot.val() === false) {
      userDoc.update({
        status: isOfflineForFirestore
      })
      return
    }

    // offline
    await rtdbRef.onDisconnect().set(isOfflineForRTDB)

    // online
    rtdbRef.set(isOnlineForRTDB)
    userDoc.update({
      status: isOnlineForFirestore
    })
  })
}

export { db, firebase, setupPresence }